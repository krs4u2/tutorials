[![Ably](https://s3.amazonaws.com/files.ably.io/logo-with-type.png)](https://www.ably.io)

---

# JWT auth tutorial

This repository contains the working code for [Ably JWT auth tutorial](https://www.ably.io/tutorials/jwt-authentication). See [JWT documentation](https://www.ably.io/documentation/general/authentication#token-authentication) to understand more about it.

See [https://www.ably.io/tutorials](https://www.ably.io/tutorials) for a complete list of Ably tutorials. The source code for each tutorial exists as a branch in this repo, see [the complete list of tutorial branches in this repository](https://github.com/ably/tutorials/branches/all).

To find out more Ably and our realtime data delivery platform, visit [https://www.ably.io](https://www.ably.io)
