FROM ubuntu:18.04
MAINTAINER krs
RUN apt-get update
# install curl
RUN apt-get install --yes curl
RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install --yes nodejs
RUN npm install pm2@latest -g
WORKDIR /usr/src

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "pm2", "start", "ecosystem.config.js" ]